<?php
require_once 'vendor/autoload.php';

// options
$opt = (object)[
  "logFile"     => "log.txt",
  "resultFile"  => "result.csv"
];



$shortopts = "a:f:";
$longopts  = array(
    "action:",
    "file:",
);

$options = getopt($shortopts, $longopts);


try{

  // get action
  if(isset($options['a'])) {
    $action = $options['a'];
  } elseif(isset($options['action'])) {
    $action = $options['action'];
  } else {
    throw new Exception("Invalid action");
  }

  // get data source
  if(isset($options['f'])) {
    $file = $options['f'];
  } elseif(isset($options['file'])) {
    $file = $options['file'];
  } else {
    throw new Exception("Invalid file name");
  }


  // init file reader
  $reader = new \FileSystem\Reader\CsvReader();
  if( !$reader->path($file) ){
    throw new Exception("Invalid file name: $file");
  }

  $math = new \Math\Math();

  $writer = new \FileSystem\Writer\CsvWriter();

  // init logger
  $logger = new \FileSystem\Logger\TxtLogger();
  if( !$logger->path($opt->logFile) ){
    throw new Exception("Can't write to {$opt->logFile}");
  }
  $logger->header = "\n=== Start $action operation ===";
  $logger->footer = "=== End $action operation ===";



  while( ($line = $reader->next()) !== null){
    $result = $math->actionFromString($action, $line);
    if($result > 0){
      $line[] = $result;
      $writer->addRow($line);
    }else{
      $logger->message("numbers {$line[0]} and {$line[1]} are wrong => $result");
    }
  }

  if( !$writer->writeTo($opt->resultFile) ){
    throw new Exception("Can't write to {$opt->resultFile}");
  }

  return 0;

}catch (Exception $e){

  echo $e->getMessage() . PHP_EOL;
  return 1;

}
