<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.19
 * Time: 16:21
 */

namespace Contract;


interface WriterInterface {


  /**
   * Add row to the buffer
  */
  public function addRow(array $row): void;

  /**
   * Write buffer to a target file
   * return true/false - success/fail
   */
  public function writeTo(string $filePath): bool;

}