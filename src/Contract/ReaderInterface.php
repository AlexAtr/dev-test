<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.19
 * Time: 16:21
 */

namespace Contract;


interface ReaderInterface {


  /**
   * Path to a data source
  */
  public function path(string $filePath): bool;

  /**
   * Get the value of the next line and increment inner counter
   * @return int[]
  */
  public function next(): ?array ;

  /**
   * Check if it's end of the file
   */
  public function eof(): bool;

}