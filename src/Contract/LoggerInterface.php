<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.19
 * Time: 16:21
 */

namespace Contract;


interface LoggerInterface {


  /**
   * Set target file path
  */
  public function path(string $filePath): bool;

  /**
   * Add row to the buffer
  */
  public function message(string $msg): void;


}