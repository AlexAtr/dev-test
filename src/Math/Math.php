<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.19
 * Time: 17:04
 */

namespace Math;


class Math{

  protected $actionsList = [
    "plus", "minus", "multiply", "division"
  ];

  protected $prerpocessors = [];

  public function actionFromString(string $action, array $values): ?float{
    $action = strtolower(trim($action));
    if( !in_array($action, $this->actionsList) ){
      throw new \Exception("Action '$action' is not supported");
    }

    return $this->$action($values);
  }


  public function plus(array $values): ?float {
    return $values[0] + $values[1];
  }


  public function minus(array $values): ?float {
    return $values[0] - $values[1];
  }


  public function multiply(array $values): ?float {
    return $values[0] * $values[1];
  }


  public function division(array $values): ?float {
    if($values[1] == 0){
      // avoid warning when devide by zero
      return $values[0] * INF;
    }
    return $values[0] / $values[1];
  }



}