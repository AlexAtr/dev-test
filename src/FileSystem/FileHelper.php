<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.19
 * Time: 17:35
 */

namespace FileSystem;

/**
 * Basic file open/close functions
*/

trait FileHelper{

  // require protected $fPointer property

  protected function closeFile() : void {
    if($this->fPointer){
      fclose($this->fPointer);
      $this->fPointer = null;
    }
  }


  public function __destruct() {
    $this->closeFile();
  }

}