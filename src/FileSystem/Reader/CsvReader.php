<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.19
 * Time: 16:39
 */

namespace FileSystem\Reader;


use Contract\ReaderInterface;
use FileSystem\FileHelper;

class CsvReader implements ReaderInterface{
  use FileHelper;

  public $delimiter = ";";

  protected $fPointer;


  /**
   * Target file path
   * return false if can't open file
  */
  public function path(string $filePath): bool {

    if( ! file_exists($filePath) ){
      return false;
    }

    $this->closeFile();
    $this->fPointer = fopen($filePath, "r");
    return true;
  }


  /**
   * Read next line from csv
  */
  public function next(): ?array {

    $payload = fgetcsv($this->fPointer, 0, $this->delimiter);
    if($payload === false) {
      return null;
    }elseif( empty($payload) ){
      // avoid empty rows
      return $this->next();
    }else{
      foreach ($payload as &$val){
        $val = intval(trim($val));
      }
      return $payload;
    }

  }


  /**
   * Check if there is end of file
  */
  public function eof(): bool {
    return $this->fPointer? feof($this->fPointer): true;
  }



}