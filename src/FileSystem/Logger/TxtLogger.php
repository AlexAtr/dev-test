<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.19
 * Time: 16:39
 */

namespace FileSystem\Logger;


use Contract\LoggerInterface;
use FileSystem\FileHelper;

class TxtLogger implements LoggerInterface {
  use FileHelper{
    closeFile as protected traitCloseFile;
  }


  public $header = "";    // new log session header
  public $footer = "";    // new log session footer


  protected $fPointer;
  protected $needHeader = true;


  /**
   * Log file path
  */
  public function path(string $filePath): bool {
    $this->closeFile();
    $this->fPointer = fopen($filePath, "w");

    return !!$this->fPointer;
  }


  /**
   * Write message to a file
  */
  public function message(string $msg): void {
    if( $this->needHeader ){
      $this->needHeader = false;
      if($this->header) $this->writeLine($this->header);
    }

    $this->writeLine($msg);
  }



  protected function writeLine(string $line): void {
    fwrite($this->fPointer, $line.PHP_EOL);
  }


  /**
   *  Override trait closeFile function
  */
  protected function closeFile(): void {
    if( $this->footer ){
      $this->writeLine($this->footer);
    }
    $this->traitCloseFile();
  }
}