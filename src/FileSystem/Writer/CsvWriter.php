<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.07.19
 * Time: 17:26
 */

namespace FileSystem\Writer;


use Contract\WriterInterface;
use FileSystem\FileHelper;

class CsvWriter implements WriterInterface {
  use FileHelper;

  public $delimiter = ";";

  protected $fPointer;
  protected $buffer = [];


  /**
   * Add row to the buffer
   */
  public function addRow(array $row): void{
    $this->buffer[] = $row;
  }

  /**
   * Write buffer to a file
   * return true/false - success/fail
   */
  public function writeTo(string $filePath): bool{

    $this->closeFile();
    $this->fPointer = fopen($filePath, "w");

    if( ! $this->fPointer ){
      return false;
    }


    foreach ($this->buffer as $fields) {
      fputcsv($this->fPointer, $fields, $this->delimiter);
    }

    $this->closeFile();

    return true;
  }

}