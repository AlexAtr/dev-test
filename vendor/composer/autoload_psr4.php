<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Math\\' => array($baseDir . '/src/Math'),
    'FileSystem\\' => array($baseDir . '/src/FileSystem'),
    'Contract\\' => array($baseDir . '/src/Contract'),
);
